<?php

namespace Drupal\jotform_block\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Client;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\Cache;


/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "jotform_block",
 *   admin_label = @Translation("Jotform Block"),
 *   category = @Translation("JotForm")
 * )
 */
class JotformBlock extends BlockBase implements ContainerFactoryPluginInterface{
  /**
    * @var \Drupal\Core\Config\ConfigFactory
    */
    protected $configFactory;
    
  /**
    * @var \GuzzleHttp\Client
    */
    private $client;

 /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('jotform_block.block_cache')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactory $configFactory, Client $client, CacheBackendInterface $cache_backend) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
    $this->client = $client;
    $this->cacheBackend = $cache_backend;

  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $block_config = $this->getConfiguration();
    $jotform_config = $this->configFactory->get('jotform_block.settings');
    $api_key = $jotform_config->get('api_key');
    $form_id = $this->configuration['jotform_form_id'];

    $cid = "form_${api_key}_${form_id}";
    $data_cached = $this->cacheBackend->get($cid);
    if (!$data_cached) {
      $uri = "https://api.jotform.com/form/${form_id}/source?apiKey=${api_key}";

      $client = \Drupal::httpClient();

      $data = $this->client->request('GET', $uri, []);
      $items = Json::decode($data->getBody()->getContents());
      $data = $items['content'];
      
      $tags = [];
      $tags = Cache::mergeTags($tags, [$cid]);

      // Store the tree into the cache.
      $this->cacheBackend->set($cid, $data, CacheBackendInterface::CACHE_PERMANENT, $tags);
    }
    else {
      $data = $data_cached->data;
      $tags = $data_cached->tags;
    }

    return [
      '#theme' => 'jotform_block_form',
      '#form_html' => $data ?? '',
      '#cache' => [
        'tags' => $tags,
        'contexts' => ['url.path']
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $jotform_config = $this->configFactory->get('jotform_block.settings');
    $api_key = $jotform_config->get('api_key');


    $uri = 'https://api.jotform.com/user/forms?apiKey=' . $api_key . '&limit=500';

    $client = \Drupal::httpClient();
    $options = [
    ];
 
    $data = $this->client->request('GET', $uri, $options);
    $items = Json::decode($data->getBody()->getContents());
    $items = $items['content'];
    $items = array_filter($items, function ($item) {
      return $item['status'] === 'ENABLED';
    });

    $options = [];
    foreach ($items as $key => $item) {
      $options[$item['id']] = $item['title'] . ' - ' . $item['id'];
    };


    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['jotform_form_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Jotform Form'),
      '#options' => $options,
      '#description' => $this->t('Select a Jotform form for this account.'),
      '#default_value' => $config['jotform_form_id'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['jotform_form_id'] = $values['jotform_form_id'];
  }

}
